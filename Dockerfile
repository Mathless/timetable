FROM openjdk:16
LABEL maintainer="rubromvit@gmail.com"
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} timetable.jar
ENTRYPOINT ["java","-Dlog4j2.formatMsgNoLookups=true","-jar","timetable.jar"]