package ru.rubik.timetable.requests.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.rubik.timetable.DTO.classes.ClassDTO;
import ru.rubik.timetable.domain.enums.Role;

import java.util.Set;

@Data
@AllArgsConstructor
public class UserUpdateRequest {
    private String username;
    private String password;
    private String name;
    private String surname;
    private String email;
    private Set<Role> roles;
    private ClassDTO schoolClass;
}
