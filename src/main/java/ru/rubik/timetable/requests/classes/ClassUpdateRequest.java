package ru.rubik.timetable.requests.classes;

import lombok.Data;

import java.util.Set;

@Data
public class ClassUpdateRequest {
    private String name;
    private Set<Long> userId;
}
