package ru.rubik.timetable.requests.classes;

import lombok.Data;

@Data
public class ClassCreateRequest {
    private String name;
}
