package ru.rubik.timetable.requests.auth;


import lombok.Data;
import ru.rubik.timetable.DTO.classes.ClassDTO;

import javax.validation.constraints.NotEmpty;

@Data
public class RegistrationRequest {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

    @NotEmpty
    private String name;

    @NotEmpty
    private String surname;

    private String email;

    @NotEmpty
    private ClassDTO schoolClass;


}
