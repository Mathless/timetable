package ru.rubik.timetable.requests.tasks;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskCreateRequest {
    private String name;
    private String description;
    private String type;
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date date;
}
