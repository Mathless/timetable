package ru.rubik.timetable.mappers;

import ru.rubik.timetable.DTO.classes.ClassShortDTO;
import ru.rubik.timetable.DTO.user.UserShortDTO;
import ru.rubik.timetable.DTO.user.UserDTO;
import ru.rubik.timetable.domain.User;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserMapper {
    public static UserDTO fromUserToDTO(User user) {
        String schoolClass;
        if(user.getSchoolClass() == null) {
            schoolClass = "";
        }
        else schoolClass = user.getSchoolClass().getName();

       return new UserDTO(user.getUsername(),
               user.getName(),
               user.getSurname(),
               user.getEmail(),
               user.getRoles(),
               new ClassShortDTO(schoolClass)
               );
    }

    public static Set<UserDTO> fromUsersToDTOs(List<User> users) {
        return users.stream()
                .map(UserMapper::fromUserToDTO)
                .collect(Collectors.toSet());
    }

    public static UserShortDTO fromUserToShortDTO(User user){
        return new UserShortDTO(user.getId(), user.getName(), user.getSurname());
    }
    public static List<UserShortDTO> fromUsersToShortDTOs(List<User> users){
        return users.stream()
                .map(UserMapper::fromUserToShortDTO)
                .collect(Collectors.toList());
    }
}
