package ru.rubik.timetable.mappers;

import ru.rubik.timetable.DTO.TaskDTO;
import ru.rubik.timetable.domain.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskMapper {
    public static TaskDTO fromTaskToDTO(Task task){
        return new TaskDTO(task.getId(), task.getName(), task.getDescription(), task.getType(), task.getDate());
    }

    public static List<TaskDTO> fromTasksToDTOs(List<Task> tasks) {
        return tasks.stream()
                .map(TaskMapper::fromTaskToDTO)
                .collect(Collectors.toList());
    }
}
