package ru.rubik.timetable.mappers;

import ru.rubik.timetable.DTO.classes.ClassShortDTO;
import ru.rubik.timetable.DTO.classes.ClassDTO;
import ru.rubik.timetable.domain.SchoolClass;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class ClassMapper {
    //map struct
    public static ClassDTO fromClassToDTO(SchoolClass schoolClass) {
        return new ClassDTO(schoolClass.getName(),
                new HashSet<>(UserMapper.fromUsersToShortDTOs(schoolClass.getUsers().stream().toList())));
    }

    public static List<ClassDTO> fromClassesToDTOs(List<SchoolClass> schoolClasses) {
        return schoolClasses.stream()
                .map(ClassMapper::fromClassToDTO)
                .collect(Collectors.toList());
    }

    public static ClassShortDTO fromClassToShortDTO(SchoolClass schoolClass) {
        return new ClassShortDTO(schoolClass.getName());
    }

    public static List<ClassShortDTO> fromClassesToShortDTOs(List<SchoolClass> schoolClasses) {
        return schoolClasses.stream()
                .map(ClassMapper::fromClassToShortDTO)
                .collect(Collectors.toList());
    }


}
