package ru.rubik.timetable.domain.enums;

public enum Role{
    STUDENT,
    HEAD_STUDENT,
    ADMIN
}
