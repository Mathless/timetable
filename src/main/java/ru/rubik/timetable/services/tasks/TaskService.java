package ru.rubik.timetable.services.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubik.timetable.DTO.TaskDTO;
import ru.rubik.timetable.controllers.TaskController;
import ru.rubik.timetable.domain.SchoolClass;
import ru.rubik.timetable.domain.Task;
import ru.rubik.timetable.domain.User;
import ru.rubik.timetable.mappers.TaskMapper;
import ru.rubik.timetable.repos.ClassRepo;
import ru.rubik.timetable.repos.TaskRepo;
import ru.rubik.timetable.repos.UserRepo;
import ru.rubik.timetable.requests.tasks.TaskCreateRequest;
import ru.rubik.timetable.requests.tasks.TaskUpdateRequest;

import java.security.Principal;
import java.util.List;

@Service
public class TaskService {
    @Autowired
    private TaskRepo taskRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ClassRepo classRepo;

    public List<TaskDTO> getTasks(Principal principal){
        User user = userRepo.findByUsername(principal.getName());
        SchoolClass schoolClass = classRepo.findByName(user.getSchoolClass().getName());

        return TaskMapper.fromTasksToDTOs(schoolClass.getTasks());
    }

    public TaskDTO getTask(Long id, Principal principal){
        User user = userRepo.findByUsername(principal.getName());
        SchoolClass schoolClass = classRepo.findByName(user.getSchoolClass().getName());

        if(taskRepo.findById(id).isPresent()){
            Task task = taskRepo.findById(id).get();

            if(task.getSchoolClass().getName().equals(schoolClass.getName())) {
                return TaskMapper.fromTaskToDTO(task);
            }
        }

        return null;
    }

    public TaskDTO createTask(TaskCreateRequest taskCreateRequest, Principal principal) {

        SchoolClass schoolClass = classRepo.findByName(
                userRepo.findByUsername(
                        principal.getName())
                        .getSchoolClass()
                        .getName()
        );

        Task task = new Task();

        task.setName(taskCreateRequest.getName());
        task.setDescription(taskCreateRequest.getDescription());
        task.setType(taskCreateRequest.getType());
        task.setDate(taskCreateRequest.getDate());
        task.setSchoolClass(schoolClass);

        schoolClass.addTask(task);

        taskRepo.save(task);

        return TaskMapper.fromTaskToDTO(task);
    }

    public TaskDTO updateTask(Long id, TaskUpdateRequest taskUpdateRequest, Principal principal) {
        SchoolClass schoolClass = classRepo.findByName(userRepo.findByUsername(principal.getName()).getName());

        if(taskRepo.findById(id).isPresent()){
            Task task = taskRepo.findById(id).get();

            if(task.getSchoolClass().getName().equals(schoolClass.getName())) {
                task.setName(taskUpdateRequest.getName());
                task.setDescription(taskUpdateRequest.getDescription());
                task.setType(taskUpdateRequest.getType());
                task.setDate(taskUpdateRequest.getDate());

                taskRepo.save(task);

                return TaskMapper.fromTaskToDTO(task);
            }
        }

        return null;
    }


    public String deleteTask(Long id) {
        if(taskRepo.existsById(id)){
            taskRepo.delete(taskRepo.getById(id));

            return "OK";
        }

        return "ERROR";
    }
}
