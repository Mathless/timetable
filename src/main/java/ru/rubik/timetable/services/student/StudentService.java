package ru.rubik.timetable.services.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubik.timetable.DTO.classes.ClassDTO;
import ru.rubik.timetable.domain.SchoolClass;
import ru.rubik.timetable.domain.User;
import ru.rubik.timetable.mappers.ClassMapper;
import ru.rubik.timetable.repos.ClassRepo;
import ru.rubik.timetable.repos.UserRepo;
import ru.rubik.timetable.utils.managers.TimeTableManager;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

@Service
public class StudentService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ClassRepo classRepo;

    @Autowired
    private TimeTableManager timeTableManager;

    public ClassDTO getStudentClass(Principal principal) {
        User user = userRepo.findByUsername(principal.getName());

        if(user == null) {
            return null; //user is not exists
        }

        boolean isExists = user.getSchoolClass() != null;

        if(!isExists) {
            return null; // user dont have any class
        }

        SchoolClass schoolClass = classRepo.findByName(user.getSchoolClass()
                .getName());

        return ClassMapper.fromClassToDTO(schoolClass);
    }

    public Map<String, List<Map<String, String>>> getSchedule(Principal principal) throws IOException {
        String classname = userRepo.findByUsername(principal.getName())
                .getSchoolClass()
                .getName();

        if(classname==null) {
            return null;
        }

        return timeTableManager.getTimetable(classname);
    }
}
