package ru.rubik.timetable.services.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.rubik.timetable.DTO.auth.AuthDTO;
import ru.rubik.timetable.DTO.auth.RegistrationDTO;
import ru.rubik.timetable.DTO.user.UserDTO;
import ru.rubik.timetable.domain.SchoolClass;
import ru.rubik.timetable.domain.User;
import ru.rubik.timetable.domain.enums.Role;
import ru.rubik.timetable.mappers.UserMapper;
import ru.rubik.timetable.repos.ClassRepo;
import ru.rubik.timetable.repos.UserRepo;
import ru.rubik.timetable.requests.auth.AuthRequest;
import ru.rubik.timetable.requests.auth.RegistrationRequest;
import ru.rubik.timetable.requests.user.UserUpdateRequest;
import ru.rubik.timetable.security.JwtProvider;

import java.util.Set;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ClassRepo classRepo;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Set<UserDTO> getUsers(){
        return UserMapper.fromUsersToDTOs(userRepo.findAll());
    }

    public UserDTO getUser(Long id){
        return UserMapper.fromUserToDTO(userRepo.findById(id).get());
    }

    public AuthDTO authUser(AuthRequest request){
        User user = userRepo.findByUsername(request.getUsername());


        if (user == null) {
            return new AuthDTO("Login is not correct");
        }

        if(!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            return new AuthDTO("Password is not correct");
        }

        System.out.println(user.getRoles());
        String token = jwtProvider.generateToken(user.getUsername());
        return new AuthDTO(user.getUsername(),
                token,
                "OK");
    }

    public RegistrationDTO registerUser(RegistrationRequest registrationRequest){
        boolean isExists = userRepo.findByUsername(registrationRequest.getUsername()) != null;

        if (isExists) {
            return new RegistrationDTO("Login is already taken");
        }

        Role role = Role.STUDENT;

        User user = new User();
        user.setUsername(registrationRequest.getUsername());
        user.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
        user.setName(registrationRequest.getName());
        user.setSurname(registrationRequest.getSurname());
        user.setEmail(registrationRequest.getEmail());
        user.addRole(role);

        SchoolClass schoolClass = classRepo.findByName(registrationRequest.getSchoolClass().getName());

        user.setSchoolClass(schoolClass);

        userRepo.save(user);

        return new RegistrationDTO("OK");
    }

    public UserDTO updateUser(Long id, UserUpdateRequest userUpdateRequest){
        User user = userRepo.findById(id).get();

        user.setUsername(userUpdateRequest.getUsername());
        user.setPassword(passwordEncoder.encode(userUpdateRequest.getPassword()));
        user.setName(userUpdateRequest.getName());
        user.setSurname(userUpdateRequest.getSurname());
        user.setEmail(userUpdateRequest.getEmail());

        userUpdateRequest.getRoles()
                        .forEach(role -> {
                            if (!user.getRoles().contains(role)) {
                                user.addRole(role);
                            }
                        });

        SchoolClass schoolClass = classRepo.findByName(userUpdateRequest.getSchoolClass().getName());

        if(schoolClass == null) return null;

        user.setSchoolClass(schoolClass);

        userRepo.save(user);

        return UserMapper.fromUserToDTO(user);
    }

    public void deleteUser(Long id) {
        if (userRepo.findById(id).isPresent()) {
            userRepo.delete(userRepo.findById(id).get());
        }
    }
}
