package ru.rubik.timetable.services.classes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubik.timetable.DTO.classes.ClassDTO;
import ru.rubik.timetable.domain.SchoolClass;
import ru.rubik.timetable.domain.User;
import ru.rubik.timetable.mappers.ClassMapper;
import ru.rubik.timetable.repos.ClassRepo;
import ru.rubik.timetable.repos.UserRepo;
import ru.rubik.timetable.requests.classes.ClassCreateRequest;
import ru.rubik.timetable.requests.classes.ClassUpdateRequest;

import java.util.List;

@Service
public class ClassService {
    @Autowired
    private ClassRepo classRepo;

    @Autowired
    private UserRepo userRepo;

    public List<ClassDTO> getClasses() {
        return ClassMapper.fromClassesToDTOs(classRepo.findAll());
    }

    public ClassDTO getOneClass(Long id) {
        boolean isExists = classRepo.findById(id).isPresent();

        if(isExists) {
            return ClassMapper.fromClassToDTO(classRepo.findById(id).get());
        }

        return null;
    }

    public ClassDTO createClass(ClassCreateRequest classCreateRequest) {
        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setName(classCreateRequest.getName());

        classRepo.save(schoolClass);

        return ClassMapper.fromClassToDTO(schoolClass);
    }

    public ClassDTO updateClass(Long id, ClassUpdateRequest classUpdateRequest) {
        if (classRepo.findById(id).isPresent()) {
            SchoolClass schoolClass = classRepo.findById(id).get();
            schoolClass.setName(classUpdateRequest.getName());
            classUpdateRequest.getUserId()
                    .forEach(userID -> {
                        if(userRepo.findById(userID).isPresent()) {
                            User user = userRepo.findById(userID).get();
                            schoolClass.addUser(user);
                        }
                    });

            classRepo.save(schoolClass);

            return ClassMapper.fromClassToDTO(schoolClass);
        }

        return null;
    }

    public void deleteClass(Long id) {
        classRepo.delete(classRepo.getById(id));
    }
}
