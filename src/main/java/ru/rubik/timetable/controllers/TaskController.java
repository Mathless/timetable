package ru.rubik.timetable.controllers;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rubik.timetable.DTO.TaskDTO;
import ru.rubik.timetable.requests.tasks.TaskCreateRequest;
import ru.rubik.timetable.requests.tasks.TaskUpdateRequest;
import ru.rubik.timetable.services.tasks.TaskService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/task")
@PreAuthorize("hasAuthority('STUDENT')")
@Api(value = "task", description = "Rest API for task operations", tags = "Task API")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping()
    public ResponseEntity<List<TaskDTO>> getTasks(Principal principal){

        return ResponseEntity.of(Optional.of(taskService.getTasks(principal)));
    }

    @GetMapping("{id}")
    public ResponseEntity<TaskDTO> getTask(@PathVariable Long id, Principal principal){

        return ResponseEntity.of(Optional.of(taskService.getTask(id, principal)));
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('HEAD_STUDENT')")
    public ResponseEntity<TaskDTO> createTask(@RequestBody @Valid TaskCreateRequest taskCreateRequest,
                                              Principal principal){

        return ResponseEntity.of(Optional.of(taskService.createTask(taskCreateRequest, principal)));
    }

    @PutMapping("{id}")
    @PreAuthorize("hasAuthority('HEAD_STUDENT')")
    public ResponseEntity<TaskDTO> updateTask(@PathVariable Long id,
                                              @RequestBody TaskUpdateRequest taskUpdateRequest,
                                              Principal principal) {

        return ResponseEntity.of(Optional.of(taskService.updateTask(id, taskUpdateRequest, principal)));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('HEAD_STUDENT')")
    public String deleteTask(@PathVariable Long id) {
        return taskService.deleteTask(id);
    }
}
