package ru.rubik.timetable.controllers;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rubik.timetable.DTO.classes.ClassDTO;
import ru.rubik.timetable.services.student.StudentService;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/student/")
@PreAuthorize("hasAuthority('STUDENT')")
@Api(value = "student", description = "Rest API for student operations", tags = "Student API")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/class")
    public ClassDTO getStudentClass(Principal principal) {
        return studentService.getStudentClass(principal);
    }

    @GetMapping("/schedule")
    public Map<String, List<Map<String, String>>> getSchedule(Principal principal) throws IOException {
        return studentService.getSchedule(principal);
    }
}
