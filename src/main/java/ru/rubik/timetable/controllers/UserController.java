package ru.rubik.timetable.controllers;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rubik.timetable.DTO.user.UserDTO;
import ru.rubik.timetable.requests.user.UserUpdateRequest;
import ru.rubik.timetable.services.user.UserService;

import java.util.Set;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasAuthority('ADMIN')")
@Api(value = "user", description = "Rest API for user operations", tags = "User API")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<Set<UserDTO>> getUsers(){
        return ResponseEntity.of(Optional.of(userService.getUsers()));
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDTO> getUsers(@PathVariable Long id){
        return ResponseEntity.of(Optional.of(userService.getUser(id)));
    }

    @PutMapping("{id}")
    public ResponseEntity<UserDTO> updateUser(@PathVariable Long id, @RequestBody UserUpdateRequest userUpdateRequest){
        return ResponseEntity.of(Optional.of(userService.updateUser(id, userUpdateRequest)));
    }

    @DeleteMapping("{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }
}
