package ru.rubik.timetable.controllers;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rubik.timetable.DTO.classes.ClassDTO;
import ru.rubik.timetable.requests.classes.ClassCreateRequest;
import ru.rubik.timetable.requests.classes.ClassUpdateRequest;
import ru.rubik.timetable.services.classes.ClassService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/class")
@PreAuthorize("hasAuthority('ADMIN')")
@Api(value = "class", description = "Rest API for class operations", tags = "Class API")
public class ClassController {
    @Autowired
    private ClassService classService;

    @GetMapping()
    public ResponseEntity<List<ClassDTO>> getClasses(){

        return ResponseEntity.of(Optional.of(classService.getClasses()));
    }

    @GetMapping("{id}")
    public ResponseEntity<ClassDTO> getOneClass(@PathVariable Long id) {

        return ResponseEntity.of(Optional.of(classService.getOneClass(id)));
    }

    @PostMapping
    public ResponseEntity<ClassDTO> create(@RequestBody ClassCreateRequest classCreateRequest) {

        return ResponseEntity.of(Optional.of(classService.createClass(classCreateRequest)));
    }

    @PutMapping("{id}")
    public ResponseEntity<ClassDTO> updateClass(@PathVariable Long id,
                                                @RequestBody ClassUpdateRequest classUpdateRequest) {

        return ResponseEntity.of(Optional.of(classService.updateClass(id, classUpdateRequest)));
    }

    @DeleteMapping("{id}")
    public void deleteClass(@PathVariable Long id) {

       classService.deleteClass(id);
    }
}
