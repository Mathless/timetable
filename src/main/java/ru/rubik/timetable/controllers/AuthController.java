package ru.rubik.timetable.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rubik.timetable.DTO.auth.AuthDTO;
import ru.rubik.timetable.DTO.auth.RegistrationDTO;
import ru.rubik.timetable.requests.auth.AuthRequest;
import ru.rubik.timetable.requests.auth.RegistrationRequest;
import ru.rubik.timetable.services.user.UserService;

import javax.validation.Valid;



@RestController
@RequestMapping("api/auth")
@Api(value = "auth", description = "Rest API for auth operations", tags = "Auth API")
public class AuthController {
    @Autowired
    private UserService userService;

    @ApiOperation(value = "Регистрация пользователя")
    @PostMapping("/register")
    public RegistrationDTO registerUser(@RequestBody @Valid RegistrationRequest registrationRequest) {

        return userService.registerUser(registrationRequest);
    }

    @ApiOperation(value = "Авторизация пользователя")
    @PostMapping("/login")
    public AuthDTO auth(@RequestBody AuthRequest request) {
        return userService.authUser(request);
    }
}
