package ru.rubik.timetable.DTO.classes;

import lombok.*;
import ru.rubik.timetable.DTO.user.UserShortDTO;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClassDTO {
    private String name;
    private Set<UserShortDTO> users;
}
