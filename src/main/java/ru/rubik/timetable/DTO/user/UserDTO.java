package ru.rubik.timetable.DTO.user;

import lombok.*;
import ru.rubik.timetable.DTO.classes.ClassShortDTO;
import ru.rubik.timetable.domain.enums.Role;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private String username;
    private String name;
    private String surname;
    private String email;
    private Set<Role> roles;
    private ClassShortDTO schoolClass;
}
