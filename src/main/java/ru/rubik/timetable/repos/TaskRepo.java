package ru.rubik.timetable.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rubik.timetable.domain.Task;

public interface TaskRepo extends JpaRepository<Task, Long> {
    Task findByName(String name);
}
