package ru.rubik.timetable.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rubik.timetable.domain.SchoolClass;

public interface ClassRepo extends JpaRepository<SchoolClass, Long> {
    SchoolClass findByName(String name);
}
