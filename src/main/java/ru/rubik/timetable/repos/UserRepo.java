package ru.rubik.timetable.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rubik.timetable.domain.User;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findBySurname(String surname);
}
