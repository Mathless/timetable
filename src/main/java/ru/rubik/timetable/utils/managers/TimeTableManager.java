package ru.rubik.timetable.utils.managers;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TimeTableManager {
    private static final String requestURL = "https://digital.etu.ru/api/mobile/schedule";

    public Map<String, List<Map<String, String>>> getTimetable(String classname) throws IOException {
        JSONObject json = doRequest();
        var map = ((JSONObject)
                ((JSONObject)
                        ((JSONObject) json.get(classname))
                ).get("days")
        );

        Map<String, List<Map<String, String>>> dayMap = new HashMap<>();

        for(String key: map.keySet()){
            JSONObject js = (JSONObject) map.get(key);
            String day = js.getString("name");
            List<Map<String, String>> lessonArray = new ArrayList<>();

            JSONArray lessons =  js.getJSONArray("lessons");

            for(Object lesson: lessons) {
                Map<String, String> lessonMap = new HashMap<>();
                lessonMap.put("week", ((JSONObject) lesson).get("week").toString());
                lessonMap.put("room", ((JSONObject) lesson).get("room").toString());
                lessonMap.put("teacher", ((JSONObject) lesson).get("teacher").toString());
                lessonMap.put("name", ((JSONObject) lesson).get("name").toString());
                lessonMap.put("start_time", ((JSONObject) lesson).get("start_time").toString());
                lessonMap.put("end_time", ((JSONObject) lesson).get("end_time").toString());
                lessonArray.add(lessonMap);
            }
            dayMap.put(day, lessonArray);
        }
        return  dayMap;
    }

    private JSONObject doRequest() throws IOException {
        URL url = new URL(requestURL);
        final HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        try (final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            final StringBuilder content = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            JSONObject json = new JSONObject(String.valueOf(content));

            return json;
        } catch (final Exception ex) {
            ex.printStackTrace();

            return null;
        }
    }
}
